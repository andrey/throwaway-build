const fs = require('fs');
const path = require('path');

const destDirName = path.resolve(__dirname, '../dist');

const readHtml = async () => {
  return await fs.promises.readFile(path.resolve(destDirName, 'index.html'), 'utf-8');
};

const renameJs = async (timestamp) => {
  const jsFilePath = path.resolve(destDirName, 'static', 'script-one.js');
  const renamedJsFilePath = path.resolve(destDirName, 'static', `script-one-${timestamp}.js`);

  await fs.promises.rename(jsFilePath, renamedJsFilePath);
};

const writeHtml = async (html) => {
  const htmlPath = path.resolve(destDirName, 'index.html');
  await fs.promises.writeFile(htmlPath, html, 'utf-8');
};

const main = async () => {
  const timestamp = Date.now();

  await renameJs(timestamp);

  let html = await readHtml();
  html = html.replace('script-one.js', `script-one-${timestamp}.js`);

  writeHtml(html);

  console.log('Done!');
};

main();
