The only pre-requisite is to have a modern version of `node` installed (a current LTS or newer).

Run `npm run build`.

You should notice a `dist` folder containing an `index.html` file and a `static` folder with a couple of files.

Every time you do a build:
- one of the javascript files (which has `script-one` in its name) will get a new timestamp added to its name
- the other javascript file will have its name unchanged
