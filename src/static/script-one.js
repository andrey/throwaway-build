import { addHeading2 } from './script-two.js';

const addHeading1 = () => {
  const el = document.createElement('h1');
  el.innerHTML = 'This heading comes from script one';
  document.body.appendChild(el);
}

addHeading1();
addHeading2();
